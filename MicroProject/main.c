/*
 * MicroProject.c
 *
 * Created: 1/12/2018 2:40:25 PM
 * Author : S.H.A.K
 */ 
#define F_CPU 8000000UL
#define BAUD_RATE 9600
#define true 1
#define false 0

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/delay.h>

int data;
char t;
//int HCounter;
//int VCounter;
char mymin, mymax;
int mmin, mmax;
ISR(ADC_vect){
	ADCSRA &= ~(1 << ADSC);
	data = ADCH;
	if(data>mmax || data<mmin){
		PORTA |= (1<<PA2);
	}
}

ISR(USART_RXC_vect){
	t = 0;
	t = UDR;
	if(t==0x6d){
		mymin=true;
		return;
	}
	if(t=='M'){
		mymax=true;
		return;
	}
	if(mymin){
		mymin=false;
		mmin=t;
		return;
	}
	if(mymax){
		mymax=false;
		mmax=t;
		return;
	}
	if(t=='a'){
		UDR=mmin;
		while(!(UCSRA & (1 << UDRE)));
		_delay_ms(100);
		return;
	}
	if(t=='A'){
		UDR=mmax;
		while(!(UCSRA & (1 << UDRE)));
		_delay_ms(100);
		return;
	}	
	UDR = data;
	while(!(UCSRA & (1 << UDRE)));
	_delay_ms(100);
	//UDR = t;
	//while(!(UCSRA & (1 << UDRE)));
	//_delay_ms(100);
	OCR0 = t;
}

ISR(TIMER1_COMPA_vect){
	PORTA |= (1 << PA1);
	TCNT1L = 0;
	TCNT1H = 0;
	ADCSRA |= (1 << ADSC);
	if(data < mmin || data > mmax)
		PORTB |= (1 << PB0);
	else
		PORTB &= ~(1 << PB0);
}

int main(void)
{
	//min-max
	mymin=false;
	mymax=false;
	mmin=5;
	mmax=30;
	DDRB |= (1 << PB0);
	PORTB &= ~(1 << PB0);
	
	//On-line LED
	DDRA |= (1 << PA1);
	PORTA |= (1 << PA1);
	
	//timer1  set to 0.5 sec interrupt to read ADC
	OCR1A = 0x1E84;
	TCNT1L = 0;
	TCNT1H = 0;
	TCCR1B |= (1<<WGM12);
	TCCR1B |= (1 << CS12) | (1 << CS10);
	TIMSK |= (1 << OCIE1A);
	
	//timer0 set for buzzer
	TCNT0 = 0;
	OCR0 = 128;
	TCCR0 = (1 << CS00) | (1 << CS02) | (1 << WGM00) | (1 << WGM01) | (1 << COM01);
	DDRB |= (1 << PB3);
	
	//Activating ADC0
	DDRA &= ~(1 << PA0);
	//set ADC0 and source to internal 2.56V and Right justified
	ADMUX |= (1 << REFS0) | (1 << REFS1) | (1 << ADLAR);
	ADMUX &= ~((1 << MUX0) | (1 << MUX1) | (1 << MUX2) | (1 << MUX3) | (1 << MUX4));
	//set Interrupt , disable auto trigger , disable start conversion , enable ADC , Prescaler to cclk\64
	ADCSRA |= (1 << ADIE) | (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1);
	ADCSRA &= ~((1 << ADATE) | (1 << ADSC) | (1 << ADPS0));
	
	//setup UART
	//set Baud Rate to 9600
	UBRRL = 0x33;
	UBRRH = 0;
	//disable 2x speed , multi processors Communication mode
	UCSRA = 0;
	//RX int enable , TX int disable , UART data register empty int disable , enable RX and TX , size 8bit data
	UCSRB = (1 << RXCIE) | (1 << RXEN) | (1 << TXEN);
	//UART mode , disable parity , one stop bit , size 8bit data
	UCSRC = (1 << URSEL) | (1 << UCSZ0) | (1 << UCSZ1);
	
	//enable interrupts
	sei();
	
    while (1);
}